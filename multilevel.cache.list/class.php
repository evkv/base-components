<?php
if (! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Iblock\Component\Tools;
use Bitrix\Iblock\IblockTable;
use Bitrix\Main;
use Bitrix\Main\Localization\Loc as Loc;
use Bitrix\Main\SystemException;

class MultilevelCacheListComponent extends CBitrixComponent
{

    /**
     * кешируемые ключи arResult
     *
     * @var array()
     */
    protected $cacheKeys = [];

    /**
     * Массив устанавливаемых тэгов для кэша
     *
     * @var array
     */
    protected $cacheTags = [];

    /**
     * дополнительные параметры, от которых должен зависеть кеш
     *
     * @var array
     */
    protected $cacheAddon = [];

    /**
     * парамтеры постраничной навигации
     *
     * @var array
     */
    protected $navParams = [];

    /**
     * возвращаемые значения
     *
     * @var mixed
     */
    protected $returned;

    /**
     * подключаемая страница шаблона. по умолчанию template (файл template.php)
     * @var string
     */
    protected $page = '';

    /**
     * @var array
     */
    protected $cacheLevels = [];

    /**
     * @var array
     */
    protected $stackLevels = [];
    /**
     * @var array
     */
    protected $arBindCacheParamsKeys = [];

    /**
     * @param string[] $tags
     * @return MultilevelCacheListComponent
     */
    protected function addCacheTag(...$tags)
    {
        foreach ($tags as $tag) {
            if (is_array($tag)) {
                $this->addCacheTag($tag);
            } else {
                if ((is_string($tag) || is_numeric($tag)) && strlen($tag)) {
                    if (!empty($this->stackLevels)) {
                        $level = end($this->stackLevels);
                        if (!is_array($this->cacheLevels[$level]['tags'])) {
                            $this->cacheLevels[$level]['tags'] = [];
                        }
                        if (!in_array($tag, $this->cacheLevels[$level]['tags'])) {
                            $this->cacheLevels[$level]['tags'][] = $tag;
                        }
                    } else {
                        if (!in_array($tag, $this->cacheTags)) {
                            $this->cacheTags[] = $tag;
                        }
                    }
                }
            }
        }
        return $this;
    }

    /**
     * @param int[] $infoBLockIds
     * @return MultilevelCacheListComponent
     */
    protected function addCacheTagIB(...$infoBLockIds)
    {
        foreach ($infoBLockIds as $infoBLockId) {
            if (is_array($infoBLockId)) {
                $this->addCacheTagIB($infoBLockId);
            } else {
                $infoBLockId = intval($infoBLockId);
                if ($infoBLockId > 0) {
                    $this->addCacheTag(sprintf('iblock_id_%d', $infoBLockId));
                }
            }
        }
        return $this;
    }

    /**
     * подключает языковые файлы
     */
    public function onIncludeComponentLang()
    {
        $this->includeComponentLang(basename(__FILE__));
        Loc::loadMessages(__FILE__);
    }

    /**
     * подготавливает входные параметры
     *
     * @param mixed $params
     *
     * @return array
     */
    public function onPrepareComponentParams($params)
    {
        $params['AJAX_KEY'] = $params['AJAX_KEY'] ? $params['AJAX_KEY'] : 'AJAX';
        $params['CACHE_HTML'] = $params['CACHE_HTML'] ?? 'VARS';
        $params['IBLOCK_TYPE'] = trim($params['IBLOCK_TYPE']);
        $params['IBLOCK_ID'] = intval($params['IBLOCK_ID']);
        $params['IBLOCK_CODE'] = trim($params['IBLOCK_CODE']);
        $params['SHOW_NAV'] = ($params['SHOW_NAV'] === 'Y' ? 'Y' : 'N');
        $params['COUNT'] = intval($params['COUNT']);
        $params['SORT_FIELD1'] = strlen($params['SORT_FIELD1']) ? $params['SORT_FIELD1'] : 'ID';
        $params['SORT_DIRECTION1'] = $params['SORT_DIRECTION1'] === 'ASC' ? 'ASC' : 'DESC';
        $params['SORT_FIELD2'] = strlen($params['SORT_FIELD2']) ? $params['SORT_FIELD2'] : 'ID';
        $params['SORT_DIRECTION2'] = $params['SORT_DIRECTION2'] === 'ASC' ? 'ASC' : 'DESC';
        $params['CACHE_TIME'] = intval($params['CACHE_TIME']) > 0 ? intval($params['CACHE_TIME']) : 3600;
        $params['AJAX'] = $params[$params['AJAX_KEY']] === 'N' ? 'N' : $_REQUEST[$params['AJAX_KEY']] === 'Y' ? 'Y' : 'N';
        $params['FILTER'] = is_array($params['FILTER']) && sizeof($params['FILTER']) ? $params['FILTER'] : [];
        $params['CACHE_TAG_OFF'] = $params['CACHE_TAG_OFF'] === 'Y';

         return $params;
    }

    /**
     * @return bool
     */
    protected function is404()
    {
        return (defined("ERROR_404") && ERROR_404 === "Y");
    }

    /**
     * @param string $message Message to show with bitrix:system.show_message component.
     * @param bool $defineConstant If true then ERROR_404 constant defined.
     * @param bool $setStatus If true sets http response status.
     * @param bool $showPage If true then work area will be cleaned and /404.php will be included.
     * @param string $pageFile Alternative file to /404.php.
     */
    protected function set404($message = "", $defineConstant = true, $setStatus = true, $showPage = false, $pageFile = "")
    {
        Tools::process404($message, $defineConstant, $setStatus, $showPage, $pageFile);
    }

    /**
     * @param mixed ...$args
     * @return $this
     */
    protected function addCacheAddon(...$args)
    {
        $this->cacheAddon[] = $args;
        return $this;
    }

    /**
     * @return array
     */
    protected function getCacheAddon()
    {
        return $this->cacheAddon;
    }
    /**
     * определяет читать данные из кеша или нет
     *
     * @return bool
     */
    protected function readDataFromCache()
    {
        if ($this->arParams['CACHE_TYPE'] === 'N') {
            return false;
        }

        return ! ($this->startResultCache(false, $this->getCacheAddon(), $this->getResultCachePath()));
    }

    /**
     *
     */
    protected function clearResultCacheComponent()
    {
        $this->clearResultCache($this->getCacheAddon(), $this->getResultCachePath());
    }

    /**
     * @param string $level
     * @return string
     */
    protected function getResultCachePath($level='')
    {
        return md5(serialize($this->getBindCacheParamsKeys()).$level);
    }

    /**
     * @return array
     */
    protected function getBindCacheParamsKeys()
    {
        if (!empty($this->arBindCacheParamsKeys) && is_array($this->arBindCacheParamsKeys)) {
            return array_intersect_key($this->arParams, array_combine($this->arBindCacheParamsKeys, $this->arBindCacheParamsKeys));
        }
        return $this->arParams;
    }

    /**
     * кеширует ключи массива arResult
     */
    protected function putDataToCache()
    {
        if (is_array($this->cacheKeys) && sizeof($this->cacheKeys) > 0) {
            $this->SetResultCacheKeys($this->cacheKeys);
        }
		$this->endCache();
    }

    /**
     * прерывает кеширование
     */
    protected function abortDataCache()
    {
        $this->AbortResultCache();
    }

    /**
     * завершает кеширование
     *
     * @return bool
     */
    protected function endCache()
    {
        if ($this->arParams['CACHE_TYPE'] === 'N') {
            return false;
        }

        $this->endResultCache();
    }

    /**
     * @return string[]
     */
    protected function includeModules()
    {
        return ['iblock'];
    }

    /**
     * проверяет подключение необходиимых модулей
     *
     * @throws Main\LoaderException
     */
    protected function checkModules()
    {
        foreach ($this->includeModules() as $includeModule) {
            if (! Main\Loader::includeModule($includeModule)) {
                throw new Main\LoaderException(Loc::getMessage('MULTILEVEL_CACHE_LIST_CLASS_IBLOCK_MODULE_NOT_INSTALLED', [
                    '#MODULE_NAME#' => $includeModule,
                ]));
            }
        }
    }
    /**
     *
     */
    protected function onBeforeIncludeComponentTemplate()
    {

    }
    /**
     * проверяет заполнение обязательных параметров
     *
     * @throws Main\ArgumentNullException
     */
    protected function checkParams()
    {
        if ($this->arParams['IBLOCK_ID'] <= 0 && strlen($this->arParams['IBLOCK_CODE']) <= 0) {
            throw new Main\ArgumentNullException('IBLOCK_ID');
        }
    }

    /**
     * выполяет действия перед кешированием
     */
    protected function executeProlog()
    {
        if ($this->arParams['COUNT'] > 0) {
            if ($this->arParams['SHOW_NAV'] === 'Y') {
                \CPageOption::SetOptionString('main', 'nav_page_in_session', 'N');
                $this->navParams = [
                    'nPageSize' => $this->arParams['COUNT'],
                ];
                $arNavigation = \CDBResult::GetNavParams($this->navParams);
                $this->addCacheAddon($arNavigation);
            } else {
                $this->navParams = [
                    'nTopCount' => $this->arParams['COUNT'],
                ];
            }
        } else {
            $this->navParams = false;
        }
        global $USER;
        $this->addCacheAddon($USER->GetUserGroupArray());
    }

    /**
     * Определяет ID инфоблока по коду, если не был задан
     */
    protected function getIblockId()
    {
        if ($this->arParams['IBLOCK_ID'] <= 0) {
            $iblock = IblockTable::getList([
                'filter' => [
                    'IBLOCK_TYPE_ID' => $this->arParams['IBLOCK_TYPE'],
                    'ACTIVE' => 'Y',
                    'CODE' => $this->arParams['IBLOCK_CODE'],
                ],
                'select' => [
                    'ID',
                ],
            ])->fetch();
            if (!empty($iblock['ID'])) {
                $this->arParams['IBLOCK_ID'] = $iblock['ID'];
            } else {
                $this->abortDataCache();
                throw new Main\ArgumentNullException('IBLOCK_ID');
            }
        }
        $this->arResult['IBLOCK_ID'] = $this->arParams['IBLOCK_ID'];
        $this->cacheKeys[] = 'IBLOCK_ID';
    }

    /**
     * получение результатов
     */
    protected function getResult()
    {
        $filter = [
            'IBLOCK_TYPE' => $this->arParams['IBLOCK_TYPE'],
            'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
            'ACTIVE' => 'Y',
        ];
        $sort = [
            $this->arParams['SORT_FIELD1'] => $this->arParams['SORT_DIRECTION1'],
            $this->arParams['SORT_FIELD2'] => $this->arParams['SORT_DIRECTION2'],
        ];
        $select = [
            'ID',
            'NAME',
            'DATE_ACTIVE_FROM',
            'DETAIL_PAGE_URL',
            'PREVIEW_TEXT',
            'PREVIEW_TEXT_TYPE',
        ];
        $iterator = \CIBlockElement::GetList($sort, $filter, false, $this->navParams, $select);
        while ($element = $iterator->GetNext()) {
            $this->arResult['ITEMS'][] = [
                'ID' => $element['ID'],
                'NAME' => $element['NAME'],
                'DATE' => $element['DATE_ACTIVE_FROM'],
                'URL' => $element['DETAIL_PAGE_URL'],
                'TEXT' => $element['PREVIEW_TEXT'],
            ];
        }
        if ($this->arParams['SHOW_NAV'] === 'Y' && $this->arParams['COUNT'] > 0) {
            $this->arResult['NAV_STRING'] = $iterator->GetPageNavString('');
        }
    }

    /**
     * выполняет действия после выполения компонента, например установка заголовков из кеша
     */
    protected function executeEpilog()
    {
        if ($this->arResult['IBLOCK_ID'] && $this->arParams['CACHE_TAG_OFF']) {
            \CIBlock::enableTagCache($this->arResult['IBLOCK_ID']);
        }
    }

    /**
     *
     */
    protected function registerCacheTags()
    {
        if (!empty($this->cacheTags)) {
            global $CACHE_MANAGER;
            foreach ($this->cacheTags as $cacheTag) {
                if ((is_string($cacheTag) || is_numeric($cacheTag)) && strlen($cacheTag)) {
                    $CACHE_MANAGER->RegisterTag($cacheTag);
                }
            }
        }
    }

    /**
     * 
     */
    protected function onBeforeReadDataFromCache()
    {

    }
    /**
     * выполняет логику работы компонента
     */
    public function executeComponent()
    {
        try {
            $this->checkModules();
            $this->checkParams();
            $this->executeProlog();
            if ($this->arParams['AJAX'] === 'Y') {
                while (ob_get_level()) {
                    ob_end_clean();
                }
            }
            $this->onBeforeReadDataFromCache();
            if (! $this->readDataFromCache()) {
                $this->getIblockId();
                $this->getResult();
                if ($this->arParams['CACHE_HTML'] === 'HTML') {
                    $this->onBeforeIncludeComponentTemplate();
                    $this->includeComponentTemplate($this->page);
                }
                if($this->is404()) {
                    $this->abortDataCache();
                } else {
                    $this->registerCacheTags();
                    $this->putDataToCache();
                }
            }
            if ($this->arParams['CACHE_HTML'] === 'VARS') {
                $this->onBeforeIncludeComponentTemplate();
                $this->includeComponentTemplate($this->page);
            }

            $this->executeEpilog();

            if ($this->arParams['AJAX'] === 'Y') {
                exit(0);
            }

        } catch (Throwable $e) {
            $this->clearResultCacheComponent();
            ShowError($e->getMessage());
        }
        return $this->returned;
    }

    /**
     * @param string $level
     * @return string
     */
    protected function getCacheAddonLevel($level='-')
    {
        if (!empty($this->cacheLevels[$level]['addon']) && is_array($this->cacheLevels[$level]['addon'])) {
            $levelCacheAddon = md5(serialize($this->cacheLevels[$level]['addon']).$level);
        } else {
            $levelCacheAddon = $this->getCacheAddon();
            if (!is_string($levelCacheAddon)) {
                $levelCacheAddon = md5(serialize($levelCacheAddon).$level);
            }
        }
        return $levelCacheAddon;
    }
    /**
     * @param string $level
     * @param mixed $default
     * @return mixed
     * @throws SystemException
     */
    protected function getCacheLevel($level='-', $default=null)
    {
        $checkMethods = [
            'addCacheLevel',
            'setCacheLevel',
        ];
        $result = null;
        if (!is_array($this->cacheLevels[$level])) {
            $this->cacheLevels[$level] = [];
        }
        $cache = Main\Application::getInstance()->getCache();
        if ($cache->startDataCache(3600000000, $this->getCacheAddonLevel($level), $this->getResultCachePath($level))) {
            foreach ($checkMethods as $checkMethod) {
                $method = $checkMethod.$level;
                if (method_exists($this, $method)) {
                    array_push($this->stackLevels, $level);
                    ob_start();
                    $result = $this->$method();
                    $this->cacheLevels[$level]['output'] = ob_get_clean();
                    array_pop($this->stackLevels);
                    break;
                }
            }
            if (isset($result) || !empty($this->cacheLevels[$level]['output'])) {
                if (!empty($this->cacheLevels[$level]['tags']) && is_array($this->cacheLevels[$level]['tags'])) {
                    Main\Application::getInstance()->getTaggedCache()->startTagCache($this->getResultCachePath($level));
                    foreach ($this->cacheLevels[$level]['tags'] as $tag) {
                        Main\Application::getInstance()->getTaggedCache()->registerTag($tag);
                    }
                    Main\Application::getInstance()->getTaggedCache()->endTagCache();
                }
                $cache->endDataCache([
                    'result' => $result,
                    'output' => $this->cacheLevels[$level]['output'],
                ]);
            } else {
                $cache->abortDataCache();
            }
        } else {
            $vars = $cache->getVars();
            if (array_key_exists('result', $vars)) {
                $result = $vars['result'];
            }
            if (array_key_exists('output', $vars)) {
                $this->cacheLevels[$level]['output'] = $vars['output'];
            }
        }
        if (isset($result)) {
            return $result;
        }
        return $default;
    }

    /**
     * @param string $level
     * @param string $default
     * @return mixed
     */
    protected function getOutputLevel($level='-', $default='')
    {
        if (isset($this->cacheLevels[$level]['output']) && is_string($this->cacheLevels[$level]['output'])) {
            return $this->cacheLevels[$level]['output'];
        }
        return $default;
    }

    /**
     * @param string $level
     * @throws SystemException
     */
    protected function clearCacheLevel($level='-')
    {
        $cache = Main\Application::getInstance()->getCache();
        $cache->clean($this->getCacheAddonLevel($level), $this->getResultCachePath($level));
    }

    /**
     * @param string $level
     * @param array $args
     */
    protected function setCacheAddonLevel($level='-', ...$args)
    {
        $this->cacheLevels[$level]['addon'] = $args;
    }

    /**
     * @param string $level
     * @param array $args
     */
    protected function addCacheAddonLevel($level='-', ...$args)
    {
        if (!is_array($this->cacheLevels[$level]['addon'])) {
            $this->cacheLevels[$level]['addon'] = $this->getCacheAddon();
        }
        $this->cacheLevels[$level]['addon'][] = $args;
    }
    /**
     * @param $name
     * @param $arguments
     * @return mixed
     * @throws Exception
     */
    public function __call($name, $arguments)
    {
        if (stripos($name,'getCacheLevel') === 0) {
            return $this->getCacheLevel(substr($name, 13), ...$arguments);
        } elseif (stripos($name,'getOutputLevel') === 0) {
            return $this->getOutputLevel(substr($name, 14), ...$arguments);
        } elseif (stripos($name,'clearCacheLevel') === 0) {
            return $this->clearCacheLevel(substr($name, 15));
        } elseif (stripos($name,'setCacheAddonLevel') === 0) {
            return $this->setCacheAddonLevel(substr($name, 18), ...$arguments);
        }
        throw new Exception(sprintf('Method %s not found', $name));
    }

}
